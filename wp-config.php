<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'platzi-base');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zA6Ni-&Ah4&R`BW**$g`^E$UioF6{FqGBYd.HBz]i&U}t!pv-wSZYIB00/T|/Ztv');
define('SECURE_AUTH_KEY',  'w+M:s0oSBCp{)#tCLE-&aW4*[glYsKw8FQ`C[[F8,aXKUH?G2&jEXN(F5.cLk0(h');
define('LOGGED_IN_KEY',    ']O]-ZFb>*H%?G=A/M1c.MS6)Sa1*2JqW8[)RvO~ZVMwvKr[ws/(`Y*z7~1&w3esl');
define('NONCE_KEY',        '8`C>^+)kve7:SfQDC3-Xu*4Mga!VI0.5GuMFx@wkF4-u1DcQq(,M%.([ZVfdvw$m');
define('AUTH_SALT',        'HwusP(nsr7<UY{zy,];~->(Tv}~W}{)T@^_:+GFL?>D(<,f476wP?L1&Kqi#)-by');
define('SECURE_AUTH_SALT', 'txEWO_UQt})4]m<IS[m$)?j#_0tv|Qb-7Rz]eq<Y_t-rAZ*IVD/)fy5[>cz0lBau');
define('LOGGED_IN_SALT',   '|^AEVs`,T<qmXPLeN$-Zn@HA1=](=>&^e<foF>$wD2Kgc(iGd^mgWdwLS{+&Ple7');
define('NONCE_SALT',       's]#>UBW%$Z9DSlBgd8]iy}gSt9^g3v(l;yK9L}uKCGjLVa}|TCh[~VE5u*fYkwdn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wps77_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
